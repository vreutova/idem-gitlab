"""
Autogenerated using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__

Epics
"""
from typing import Any
from typing import Dict

import dict_tools.differ as differ


async def present(
    hub,
    ctx,
    name: str,
    project_id: int,
    title: str,
    labels: str = None,
    description: str = None,
    confidential: bool = None,
    created_at: str = None,
    start_date_is_fixed: bool = None,
    start_date_fixed: str = None,
    due_date_is_fixed: bool = None,
    due_date_fixed: str = None,
    parent_id: int = None,
) -> Dict[str, Any]:
    r"""
    **Autogenerated function**

    Creates a new epic.


    Args:
        name(Text): The identifier for this state.
        project_id(int): The ID or URL-encoded path of the group owned by the authenticated user.
        title(str): The title of the epic.
        labels(str, optional): The comma separated list of labels. Defaults to None.
        description(str, optional): The description of the epic. Limited to 1,048,576 characters. Defaults to None.
        confidential(bool, optional): Whether the epic should be confidential. Defaults to None.
        created_at(str, optional): When the epic was created. Date time string, ISO 8601 formatted, for example 2016-03-11T03:45:40Z . Requires administrator or project/group owner privileges (available in GitLab 13.5 and later). Defaults to None.
        start_date_is_fixed(bool, optional): Whether start date should be sourced from start_date_fixed or from milestones (in GitLab 11.3 and later). Defaults to None.
        start_date_fixed(str, optional): The fixed start date of an epic (in GitLab 11.3 and later). Defaults to None.
        due_date_is_fixed(bool, optional): Whether due date should be sourced from due_date_fixed or from milestones (in GitLab 11.3 and later). Defaults to None.
        due_date_fixed(str, optional): The fixed due date of an epic (in GitLab 11.3 and later). Defaults to None.
        parent_id(int, optional): The ID of a parent epic (in GitLab 11.11 and later). Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:

        .. code-block:: sls

            resource_is_present:
              gitlab.group.epics.present:
                - name: value
                - project_id: value
                - title: value
    """

    result = dict(comment="", changes=None, name=name, result=True)

    before = hub.exec.request.json.get(
        ctx,
        url=f"{ctx.acct.endpoint_url}/groups/{project_id}/epics",
        data={},
        success_codes=[200],
    )
    if before["status"]:
        result["comment"] = f"'{name}' already exists"
    else:
        ret = await hub.exec.request.json.post(
            ctx,
            success_codes=[201, 304, 204],
            url=f"{ctx.acct.endpoint_url}/groups/{project_id}/epics",
            **{
                "title": title,
                "labels": labels,
                "description": description,
                "confidential": confidential,
                "created_at": created_at,
                "start_date_is_fixed": start_date_is_fixed,
                "start_date_fixed": start_date_fixed,
                "due_date_is_fixed": due_date_is_fixed,
                "due_date_fixed": due_date_fixed,
                "parent_id": parent_id,
            },
        )
        result["result"] = ret["status"]
        if not result["result"]:
            result["comment"] = ret["comment"]
            return result
        result["comment"] = f"Created '{name}'"

    # Now that the resource exists, update it
    ret = await hub.exec.request.json.put(
        ctx,
        url=f"{ctx.acct.endpoint_url}/groups/{project_id}/epics",
        success_codes=[200, 204, 304],
    )

    if not ret["status"]:
        result["status"] = False
        result["comment"] = f"Unable to update '{name}': {ret['comment']}"

    after = hub.exec.request.json.get(
        ctx,
        url=f"{ctx.acct.endpoint_url}/groups/{project_id}/epics",
        data={},
        success_codes=[200],
    )
    result["changes"] = differ.deep_diff(before["ret"], after["ret"])
    return result


async def absent(hub, ctx, name: str, project_id: int, epic_iid: int) -> Dict[str, Any]:
    r"""
    **Autogenerated function**

    Creates a new epic.


    Args:
        name(Text): The identifier for this state.
        project_id(int): The ID or URL-encoded path of the group owned by the authenticated user.
        epic_iid(int): The internal ID of the epic.

    Returns:
        Dict[str, Any]

    Examples:

        .. code-block:: sls

            resource_is_absent:
              gitlab.group.epics.absent:
                - name: value
                - project_id: value
                - epic_iid: value
    """

    result = dict(comment="", changes=None, name=name, result=True)
    before = hub.exec.request.json.get(
        ctx,
        url=f"{ctx.acct.endpoint_url}/groups/{project_id}/epics",
        data={},
        success_codes=[204, 304, 404],
    )

    if before["status"]:
        result["comment"] = f"'{name}' already absent"
    else:
        ret = await hub.exec.request.json.delete(
            ctx,
            url=f"{ctx.acct.endpoint_url}/groups/{project_id}/epics",
            success_code=[204],
            **{"epic_iid": epic_iid},
        )
        result["result"] = ret["status"]
        if not result["result"]:
            result["comment"] = ret["comment"]
            return result
        result["comment"] = f"Deleted '{name}'"

    after = hub.exec.request.json.get(
        ctx,
        url=f"{ctx.acct.endpoint_url}/groups/{project_id}/epics",
        data={},
        success_codes=[204, 304, 404],
    )

    result["changes"] = differ.deep_diff(before["ret"], after["ret"])
    return result


async def describe(hub, ctx) -> Dict[str, Dict[str, Any]]:
    r"""
    **Autogenerated function**

    Describe the resource in a way that can be recreated/managed with the corresponding "present" function


    Gets all epics of the requested group and its subgroups.



    Returns:
        Dict[str, Any]

    Examples:

        .. code-block:: bash

            $ idem describe gitlab.group.epics
    """

    result = {}

    async for project in hub.exec.gitlab.request.paginate(
        ctx, url=f"{ctx.acct.endpoint_url}/groups"
    ):
        project_id = project["id"]

        async for ret in hub.exec.gitlab.request.paginate(
            ctx, url=f"{ctx.acct.endpoint_url}/groups/{project_id}/epics"
        ):
            result[f"/groups-{project_id}-group.epics-{ret['id']}"] = {
                "gitlab.group.epics.present": [
                    {"project_id": ret.get("id")},
                    {"title": ret.get("title")},
                    {"labels": ret.get("labels")},
                    {"description": ret.get("description")},
                    {"confidential": ret.get("confidential")},
                    {"created_at": ret.get("created_at")},
                    {"start_date_is_fixed": ret.get("start_date_is_fixed")},
                    {"start_date_fixed": ret.get("start_date_fixed")},
                    {"due_date_is_fixed": ret.get("due_date_is_fixed")},
                    {"due_date_fixed": ret.get("due_date_fixed")},
                    {"parent_id": ret.get("parent_id")},
                ]
            }

    return result
